# Libraries ---------------------------------------------------------------
library(parallel)
library(dplyr)



# Set input and output dir ------------------------------------------------
dir_in <- "I:/SCIENCE-PLEN-ECP-AnalytChem/Projects/FoodTranscriptomics/data/Method Development_Xevo/Raw"
dir_out <- "I:/SCIENCE-PLEN-ECP-AnalytChem/Projects/FoodTranscriptomics/data/Method Development_Xevo/Converted"



# Do the conversion -------------------------------------------------------
# Make output folder if it doesn't exist
dir.create(dir_out, showWarnings = FALSE, recursive = TRUE)

# Get all raw folders
files_in <- list.dirs(dir_in) %>% {grep(".raw",., value = TRUE, fixed=TRUE)}

# remove files that already exist
file_exist <- files_in %>% basename %>% {gsub(".raw$","",.)} %>% {paste0(dir_out,"/",.,".mzML")} %>% file.exists
files_in <- files_in[!file_exist]

# Generate command for each file
cmd <- paste0('msconvert.exe "',files_in,'" --mzML --zlib --64 --outdir "',dir_out,'"')

# Do parallel conversion
cl <- makeCluster(6) # number of cores. use detectCores() to get all available
out <- parSapply(cl,cmd, function(x) system(x, intern=TRUE))
stopCluster(cl)
