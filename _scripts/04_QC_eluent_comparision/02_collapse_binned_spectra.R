# Notes -------------------------------------------------------------------
# combineSpectra gives strange results. I don't think it was mean for this

# libraries ---------------------------------------------------------------
library(MSnbase)
library(dplyr)
library(purrr)
library(tidyr)
library(future)
library(furrr)
library(ggplot2)
library(scales)
library(plotly)
library(Rplot.extra)



# Funs --------------------------------------------------------------------
binwrap <- function(raw, binSize, intCut){
    raw %>% 
        removePeaks(intCut) %>% 
        clean(all=TRUE) %>% 
        filterEmptySpectra %>% 
        bin(binSize = binSize) %>% 
        removePeaks(intCut) %>% 
        clean(all=TRUE) %>% 
        filterEmptySpectra
}



sumSpectra <- function(raw, binSize, intCut, rt){
    raw %>% 
        filterRt(rt = rt) %>% 
        binwrap(binSize = binSize, intCut = intCut) %>% 
        {tibble(mz = mz(.), intensity = intensity(.))} %>% 
        unnest %>% 
        group_by(mz) %>% 
        summarise(intensity = sum(intensity)) %>% 
        {new("Spectrum2", intensity = pull(., intensity), mz = pull(., mz), centroided = TRUE)}
}





# make summed spectra -----------------------------------------------------
options(future.globals.maxSize= 4*1024^3)
cl <- parallel::makeCluster(detectCores())
plan(cluster, workers = cl)


sumSpectra <- raw %>% 
                pData %>% 
                filter(Experiment=="pH" & (`Sample Type`=="QC" | `Sample Type`=="Blank")) %>% 
                # slice(1) %>% 
                mutate(raw = map(filepath, ~ filterFile(raw, pData(raw) %>% {.$filepath==..1} %>% which))) %>% # splitByFile doesn't work with MsnExp yet it seems
                # mutate(spectra = map(raw, ~ Spectra(spectra(..1)))) #%>% 
                # mutate(spectrum_comb = map(spectrum, ~ combineSpectra(..1, intensityFun=sum, ppm=10, mzd = 0.01)))
                mutate(sumSpectra = future_map(raw, sumSpectra, binSize = 0.1, intCut = 300, rt = c(0,60*18)))



stopCluster(cl)
rm(cl)


# Average spectra from "same" experiments ---------------------------------
sumSpectra <- sumSpectra %>% 
                    mutate(sumSpectra_tab = map(sumSpectra, ~ tibble(mz = mz(..1), intensity = intensity(..1)) )) %>% 
                    select(-raw, -sumSpectra) %>% 
                    unnest(sumSpectra_tab)
    
    

sumSpectra_mean <- sumSpectra %>%
                    group_by(ion_mode, `Sample Type`, Eluent, mz) %>% 
                    summarise(intensity = mean(intensity)) %>% 
                    ungroup


# Subtract blank ----------------------------------------------------------
sumSpectra_mean_bsub <- sumSpectra_mean %>% 
                            spread(`Sample Type`, intensity) %>% 
                            mutate_at(vars(Blank, QC), funs(replace_na(., 0))) %>% 
                            mutate(intensity = QC-Blank) %>% 
                            select(-Blank, -QC)


# TODO
# sumSpectra_bsub <- sumSpectra %>% 
#                             spread(`Sample Type`, intensity) %>% 
#                             mutate_at(vars(Blank, QC), funs(replace_na(., 0))) %>% 
#                             mutate(intensity = QC-Blank) %>% 
#                             select(-Blank, -QC)



# Plot spectra ------------------------------------------------------------

p <- sumSpectra_mean_bsub %>% 
        filter(intensity>300) %>% # some blanks contain more than the QC so we get large negative signals
        mutate(Eluent = factor(Eluent, c("0.1 % FA", "1 % FA", "ACN 1 % FA", "2.5 % FA", "5 % FA", "pH 4", "pH 6", "pH 9"))) %>%
        ggplot(data = ., aes(mz,ymax=intensity, ymin=0)) +
        geom_linerange() +
        theme_bw() +
        facet_grid(Eluent ~ ion_mode) +
        # scale_y_log10(minor_breaks = log10_minor_break(),
        #                       labels = trans_format("log10", math_format(10^.x)),
        #                       expand = c(0, 0)
        #                       )
        scale_x_continuous(breaks=seq(0,1000,100))

paste0("_output/", params$general$date,"/plots/QCs/collapsed_spectra/spectra.png")



# PCA ---------------------------------------------------------------------

p_data <- sumSpectra_mean_bsub %>%
            group_by(ion_mode, mz) %>%
            filter(!all(intensity<0 | is.na(intensity))) %>% 
            filter(sum(!is.na(intensity)) > 4) %>% 
            ungroup %>% 
            mutate(bg_col = "grey", bg_lab = "", mz = as.character(mz))


# NEG
p <- p_data %>% 
    # mutate(bg_col = as.numeric(mz)) %>%
    mutate(bg_col = "grey") %>%
    mutate(fg_colors = "black") %>% 
    filter(ion_mode == "negative") %>% 
    tidy_PCA(data = .,
             sample_col = Eluent, 
             var_col = mz, 
             value_col = intensity, 
             pc_max = 2, 
             scale = "uv",
             method = "nipals"
      ) %>% 
        tidy_PCA_plot(data = .,
                      sample_col = Eluent,
                      var_col = mz,
                      fg_labels = Eluent,
                      fg_colors = fg_colors,
                      fg_colors_literal = TRUE,
                      bg_labels = bg_lab, 
                      bg_colors = bg_col,
                      bg_colors_literal = TRUE,
                      foreground = "scores",
                      fg_geom = "lines",
                      bg_geom = "dots",
                      text_offset = 0.005,
                      pcs = c(1, 2),
                      loading_quantile = 0.99
      )

p$layers[[4]]$aes_params$size <- 7
p$layers[[1]]$aes_params$alpha=0.8

ggsave(plot = p, paste0("_output/", params$general$date,"/plots/QCs/collapsed_spectra/PCA_neg.png"), width = 12, height = 8)




# POS
p <- p_data %>% 
    # mutate(bg_col = as.numeric(mz)) %>%
    mutate(bg_col = "grey") %>%
    mutate(fg_colors = "black") %>% 
    filter(ion_mode == "positive") %>% 
    tidy_PCA(data = .,
             sample_col = Eluent, 
             var_col = mz, 
             value_col = intensity, 
             pc_max = 2, 
             scale = "uv",
             method = "nipals"
      ) %>% 
        tidy_PCA_plot(data = .,
                      sample_col = Eluent,
                      var_col = mz,
                      fg_labels = Eluent,
                      fg_colors = fg_colors,
                      fg_colors_literal = TRUE,
                      bg_labels = bg_lab, 
                      bg_colors = bg_col,
                      bg_colors_literal = TRUE,
                      foreground = "scores",
                      fg_geom = "lines",
                      bg_geom = "dots",
                      text_offset = 0.005,
                      pcs = c(1, 2),
                      loading_quantile = 0.99
      )

p$layers[[4]]$aes_params$size <- 7
p$layers[[1]]$aes_params$alpha=0.8

ggsave(plot = p, paste0("_output/", params$general$date,"/plots/QCs/collapsed_spectra/PCA_pos.png"), width = 12, height = 8)



rm(p_data, p)

# Trials ------------------------------------------------------------------
# ls() %>% tibble(object = .) %>% mutate(size = map_dbl(object,~ object_size(get(..1)))) %>% arrange(desc(size))
# Sys.time()

