# Libraries ---------------------------------------------------------------
library(ggplot2)



# QC rsd dist -------------------------------------------------------------
QC_feature_sum <- peaklist_pH_exp_all_long %>%
                        filter(`Sample Type`=="QC") %>%
                        select(feature_id, polarity, into, into_drift_corr) %>% 
                        gather("intensity", "value", -feature_id, -polarity) %>% 
                        group_by(feature_id, polarity, intensity) %>% 
                        summarise(n = n(), mean = mean(value), sd = sd(value), rsd = sd/mean) %>% 
                        ungroup



QC_feature_sum %>%
    {
    ggplot(data=., aes(x = abs(rsd), group=intensity, color=intensity, fill=intensity)) +
            geom_density(aes(y = ..count..), alpha = 0.5, n = 1e4) + 
            theme_bw() +
            facet_grid(polarity~., scales = "free_y") +
            coord_cartesian(xlim = c(0, 2)) +
            theme(legend.position = "bottom") +
            theme(axis.text = element_text(size=20)) +
            scale_x_continuous(labels = scales::percent_format(accuracy = 1))
    } %>% 
ggsave(plot=., paste0("_output/", params$general$date,"/plots/QCs/wine_QC_rsd_dist.png"), width = 10, height = 10)


rm(QC_feature_sum)




# QC rsd dist -------------------------------------------------------------
QC_feature_sum <- peaklist_pH_exp_all_long %>%
                        filter(`Sample Type`=="QC") %>%
                        select(pcgroup, feature_id, polarity, into, into_drift_corr) %>% 
                        gather("intensity", "value", -feature_id, -polarity, -pcgroup) %>% 
                        group_by(feature_id, pcgroup, polarity, intensity) %>% 
                        summarise(n = n(), mean = mean(value), sd = sd(value), rsd = sd/mean) %>% 
                        ungroup %>% 
                        group_by(pcgroup, polarity, intensity) %>% 
                        summarise(rsd = min(abs(rsd), na.rm = TRUE)) %>% 
                        ungroup



QC_feature_sum %>%
    {
    ggplot(data=., aes(x = abs(rsd), group=intensity, color=intensity, fill=intensity)) +
            geom_density(aes(y = ..count..), alpha = 0.5, n = 1e4) + 
            theme_bw() +
            facet_grid(polarity~., scales = "free_y") +
            coord_cartesian(xlim = c(0, 2)) +
            theme(legend.position = "bottom") +
            theme(axis.text = element_text(size=20)) +
            scale_x_continuous(labels = scales::percent_format(accuracy = 1))
    } %>% 
ggsave(plot=., paste0("_output/", params$general$date,"/plots/QCs/wine_QC_rsd_dist_best_in_group.png"), width = 10, height = 10)






# QC rsd dist -------------------------------------------------------------
QC_feature_sum <- peaklist_pH_exp_all_long %>%
                        filter(`Sample Type`=="QC") %>%
                        select(pcgroup, feature_id, polarity, into, into_drift_corr) %>% 
                        gather("intensity", "value", -feature_id, -polarity, -pcgroup) %>% 
                        group_by(feature_id, pcgroup, polarity, intensity) %>% 
                        summarise(n = n(), mean = mean(value), sd = sd(value), rsd = sd/mean) %>% 
                        ungroup %>% 
                        group_by(pcgroup, polarity, intensity) %>% 
                        filter(mean==max(mean)) %>% 
                        ungroup



QC_feature_sum %>%
    {
    ggplot(data=., aes(x = abs(rsd), group=intensity, color=intensity, fill=intensity)) +
            geom_density(aes(y = ..count..), alpha = 0.5, n = 1e4) + 
            theme_bw() +
            facet_grid(polarity~., scales = "free_y") +
            coord_cartesian(xlim = c(0, 2)) +
            theme(legend.position = "bottom") +
            theme(axis.text = element_text(size=20)) +
            scale_x_continuous(labels = scales::percent_format(accuracy = 1))
    } %>% 
ggsave(plot=., paste0("_output/", params$general$date,"/plots/QCs/wine_QC_rsd_dist_highest_in_group.png"), width = 10, height = 10)





QC_feature_sum %>% filter(intensity=="into_drift_corr") %>% 
    {
    ggplot(data=., aes(x = abs(rsd), group=intensity, color=intensity, fill=intensity)) +
            geom_density(aes(y = ..count..), alpha = 1, n = 1e4) + 
            theme_bw() +
            facet_grid(polarity~., scales = "free_y") +
            coord_cartesian(xlim = c(0, 2)) +
            theme(legend.position = "bottom") +
            theme(axis.text = element_text(size=20)) +
            scale_x_continuous(labels = scales::percent_format(accuracy = 1))
    } %>% 
ggsave(plot=., paste0("_output/", params$general$date,"/plots/QCs/wine_QC_rsd_dist_highest_in_group_corr_only.png"), width = 10, height = 10)


rm(QC_feature_sum)



