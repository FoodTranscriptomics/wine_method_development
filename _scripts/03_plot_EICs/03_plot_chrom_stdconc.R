# Libraries ---------------------------------------------------------------
library(tidyr)
library(gridExtra)
library(ggplot2)
library(dplyr)
library(xcms)
library(purrr)
library(rlist)
library(writexl)



# Nest data for each compound ---------------------------------------------
EICs_long_nest <- EICs_long %>% nest(-interval_name)



# Get peaklist ------------------------------------------------------------
peaklist_pH <- map_dfr(x,
                            ~..1 %>% 
                            list.remove("all") %>% 
                            map_dfr(~ chromPeaks(..1$pH_exp$pgagf)  %>% 
                                        as.data.frame() %>% 
                                        bind_cols(as.data.frame(chromPeakData(..1$pH_exp$pgagf))) %>% 
                                        as_tibble %>% 
                                        mutate(peakidx = 1:nrow(.)) %>% 
                                        dplyr::rename(fromFile = sample) %>% 
                                        mutate(filepath = fileNames(..1$pH_exp$pgagf)[fromFile]), 
                                    .id = "Eluent"
                                    ),
                     .id = "polarity"
                     ) %>% 
                mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                mutate(polarity = ifelse(polarity == "neg","negative",polarity))
                


# Concentrated std only ----------------------------------------------------------------
tbl_list <- list()
pdf(paste0("_output/", params$general$date,"/plots/EICs/chroms2_stdconc.pdf"), width = 11.7*2, height=8.3*2)

for(i in 1:nrow(EICs_long_nest)){


    p <- EICs_long_nest$data[[i]] %>%
            mutate(Eluent = factor(Eluent, c("0.1 % FA", "1 % FA", "ACN 1 % FA", "2.5 % FA", "5 % FA", "pH 4", "pH 6", "pH 9"))) %>%
            filter(`Sample Type` == "Std Mix Conc") %>%
            ggplotChromatograms(rt_unit = "min") +
            aes(group=file, color=Date) +
            theme(legend.position = "bottom") +
            facet_wrap(~ polarity_interval + Eluent, ncol=2, scales="free_y", dir="v", strip.position = "right") +
            ggtitle(EICs_long_nest$interval_name[[i]]) +
            scale_x_continuous(minor_breaks = seq(0 , 100, 1), breaks = seq(0, 100, 5)) +
            theme(panel.grid.major.x = element_line(colour="black", size=0.5),
                  panel.grid.minor.x = element_line(colour="lightgrey", size=0.5)
                  )

    q <- ggplot_build(p)
    q$data[[1]]$size <- rep(0.1, length(q$data[[1]]$size))
    # q <- ggplot_gtable(q)
    
    print(q$plot)



    tbl_list[[i]] <- mz_intevals %>% 
                        mutate(interval_name = Name, int_idx = 1:n()) %>%
                        filter(int_idx %in% unique(EICs_long_nest$data[[i]]$int_idx)) %>% 
                        select(mz_upper, mz_lower, int_idx, polarity, Name = interval_name) %>% 
                        right_join(peaklist_pH, by = "polarity") %>% 
                        filter(mz>mz_lower, mz<mz_upper) %>% 
                        filter(is_filled==0) %>% 
                        left_join(factors, by = c("filepath", "Eluent")) %>% 
                        filter(`Sample Type` == "Std Mix Conc", Experiment == "pH") %>% 
                        mutate_at(vars(rt, rtmin, rtmax), funs(./60)) %>% 
                        mutate(file = basename(filepath)) %>%
                        mutate(minperscan=rt/mu, FWHM = 2*sqrt(2*log(2))*sigma*minperscan) %>% 
                        select(int_idx, file, `Sample Type`, Eluent, mz_upper, mz_lower , mz, mzmin, mzmax, rt, rtmin, rtmax, into, intb, maxo, FWHM, sn, polarity, Name )
    
    
}
dev.off()



tbl_pH_stdconc <- bind_rows(tbl_list, .id = "compound") %>% 
                    arrange(compound, Eluent, polarity, file, rt)



# Write output ------------------------------------------------------------
write_xlsx(tbl_pH_stdconc, paste0("_output/", params$general$date,"/tables/pH_EIC.tab_stdconc.xlsx"))

rm(peaklist_pH, p, q, EICs_long_nest, tbl_list, i)
