# Library -----------------------------------------------------------------
library(xcms)
library(rlist)
library(listviewer)



# Peak picking ------------------------------------------------------------
x_p_all <- findChromPeaks(raw, param = params$CentWave, BPPARAM = SnowParam(workers = detectCores(), type = "SOCK"))

saveRDS(x_p_all, paste0("_output/", params$general$date,"/data/picked_peaks.rds"))

# plotChromPeakImage(x_p_all, binSize = 10, log=TRUE)
# plotChromPeaks(x_p_all, file = 10)


# Split positive and neg mode ---------------------------------------------
x <- list()

# Split according to mode
x$pos$all$p <- filterFile(x_p_all, file = pData(x_p_all) %>% {"positive" == .$ion_mode} %>% which)
x$neg$all$p <- filterFile(x_p_all, file = pData(x_p_all) %>% {"negative" == .$ion_mode} %>% which)

# Split according to eluent group                  
for(i in unique(pData(x_p_all)[["Eluent"]])){
    x$pos[[i]]$all_types$p <- filterFile(x$pos$all$p, file = pData(x$pos$all$p) %>% {i == .$Eluent} %>% which)
    x$neg[[i]]$all_types$p <- filterFile(x$neg$all$p, file = pData(x$neg$all$p) %>% {i == .$Eluent} %>% which)
}

# Get blanks, QCs and stds together
for(i in unique(pData(x_p_all)[["Eluent"]])){
    x$pos[[i]]$pH_exp$p <- filterFile(x$pos[[i]]$all_types$p, file = pData(x$pos[[i]]$all_types$p) %>% {grep("^QC$|^Blank$|^Std Mix$",.$`Sample Type`)})
    x$neg[[i]]$pH_exp$p <- filterFile(x$neg[[i]]$all_types$p, file = pData(x$neg[[i]]$all_types$p) %>% {grep("^QC$|^Blank$|^Std Mix$",.$`Sample Type`)})
}


# 1 % was run many times. take only the ones from the pH experiment
x$pos$`1 % FA`$pH_exp$p <- filterFile(x$pos$`1 % FA`$pH_exp$p, 
                                        file = pData(x$pos$`1 % FA`$pH_exp$p) %>% {"2018_10_15" == .$Date | "2018_10_19" == .$Date} %>% which
                                        )

x$neg$`1 % FA`$pH_exp$p <- filterFile(x$neg$`1 % FA`$pH_exp$p, 
                                        file = pData(x$neg$`1 % FA`$pH_exp$p) %>% {"2018_10_15" == .$Date | "2018_10_19" == .$Date} %>% which
                                        )



# rapply(x, class, how="list") %>% jsonedit


# Count samples in the QC/Blank set ---------------------------------------

map_dfr(x,
    ~ ..1 %>% 
        list.remove("all") %>% 
        map_dfr(~ ..1$pH_exp$p %>%
                    pData %>% 
                    group_by(ion_mode, `Sample Type`) %>% 
                    summarise(n = n()) %>% 
                    ungroup,
                .id = "Eluent"),
    .id = "polarity") %>% 
    spread(`Sample Type`, n)







# Alignment ---------------------------------------------------------------
# Do grouping for all sets of data
for( i in names(x$pos)){
    if( i == "all") next
    
    for( ii in names(x$pos[[i]])){
        
        sampleGroups(params$PeakDensity1$pos[[i]]) <- x$pos[[i]][[ii]]$p %>% pData %>% {rep("dummy", nrow(.))}
        x$pos[[i]][[ii]]$pg <- groupChromPeaks(x$pos[[i]][[ii]]$p, param = params$PeakDensity1$pos[[i]])
        
        sampleGroups(params$PeakDensity1$neg[[i]]) <- x$neg[[i]][[ii]]$p %>% pData %>% {rep("dummy", nrow(.))}
        x$neg[[i]][[ii]]$pg <- groupChromPeaks(x$neg[[i]][[ii]]$p, param = params$PeakDensity1$neg[[i]])
    
    }
}

# highlightChromPeaks(x$neg$pg)


# RT aligment
for( i in names(x$pos)){
    if( i == "all") next
    
    for( ii in names(x$pos[[i]])){
        
        subset(params$PeakGroups) <- x$pos[[i]][[ii]]$pg %>% pData %>% pull(`Sample Type`) %>% grep("Blank", .)
        x$pos[[i]][[ii]]$pga <- adjustRtime(x$pos[[i]][[ii]]$pg, param = params$PeakGroups)
    
        subset(params$PeakGroups) <- x$neg[[i]][[ii]]$pg %>% pData %>% pull(`Sample Type`) %>% grep("Blank", .)
        x$neg[[i]][[ii]]$pga <- adjustRtime(x$neg[[i]][[ii]]$pg, param = params$PeakGroups) 
        
        
        subset(params$PeakGroups) <- integer(0)
    }
}



# grouping again
for( i in names(x$pos)){
    if( i == "all") next
    
    for( ii in names(x$pos[[i]])){
        
        sampleGroups(params$PeakDensity2$pos[[i]]) <- x$pos[[i]][[ii]]$p %>% pData %>% {rep("dummy", nrow(.))}
        x$pos[[i]][[ii]]$pgag <- groupChromPeaks(x$pos[[i]][[ii]]$pga, param = params$PeakDensity2$pos[[i]])
        
        sampleGroups(params$PeakDensity2$neg[[i]]) <- x$neg[[i]][[ii]]$p %>% pData %>% {rep("dummy", nrow(.))}
        x$neg[[i]][[ii]]$pgag <- groupChromPeaks(x$neg[[i]][[ii]]$pga, param = params$PeakDensity2$neg[[i]])
    
    }
}


#plotAdjustedRtime(x$pos$pga, col = as.factor(sampleGroups(params$PeakDensity1$pos)), peakGroupsCol = "grey", peakGroupsPch = 1) 


# Gap filling -------------------------------------------------------------
for( i in names(x$pos)){
    if( i == "all") next
    
        for( ii in names(x$pos[[i]])){
            x$pos[[i]][[ii]]$pgagf <- fillChromPeaks(x$pos[[i]][[ii]]$pgag, params$FillChromPeaks, BPPARAM = SnowParam(8))
            x$neg[[i]][[ii]]$pgagf <- fillChromPeaks(x$neg[[i]][[ii]]$pgag, params$FillChromPeaks, BPPARAM = SnowParam(8))
        }
}



# Get old style XCMSset ---------------------------------------------------
for( i in names(x$pos)){
    if( i == "all") next
    
    for( ii in names(x$pos[[i]])){
    x$pos[[i]][[ii]]$pgagf_xset <- as(x$pos[[i]][[ii]]$pgagf, "xcmsSet")
    x$neg[[i]][[ii]]$pgagf_xset <- as(x$neg[[i]][[ii]]$pgagf, "xcmsSet")
    
    }
}



# Save all data -----------------------------------------------------------
rm(i, ii)

saveRDS(x,       paste0("_output/", params$general$date,"/data/picked_peaks_grouped_filled.rds"))
