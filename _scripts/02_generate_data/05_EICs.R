# Libraries ---------------------------------------------------------------
library(googlesheets)
library(MSnbase)
library(dplyr)
library(ggforce)
library(rinchi)
library(rcdk)
library(purrr)
library(tidyr)

source("_funs/FUNS.R")


# Funs --------------------------------------------------------------------
do.typing_p <- possibly(do.typing, NA)
do.aromaticity_p <- possibly(do.aromaticity, NA)
do.isotopes_p <-  possibly(do.isotopes, NA)

mol_2_exact_mass <- function(x){
    do.typing_p(x)
    do.aromaticity_p(x)
    do.isotopes_p(x)

    mass <- get.exact.mass(x)

    return(mass)
}



# Read google sheet -------------------------------------------------------
gdoc <- gs_title("wine_compounds", verbose = TRUE)
data <- gs_read(gdoc, ws = "Data_extended")



# Make chromatograms for both stds and wine -------------------------------
raw_selected <- filterFile(raw, file = pData(raw) %>% {.$Experiment == "pH"} %>% which)

pheno <- fData(raw_selected) %>%
            select(fileIdx , polarity) %>%
            distinct %>%
            right_join(pData(raw_selected) %>% mutate(fileIdx = 1:n()), by = "fileIdx") %>%
            as_tibble %>%
            mutate(polarity = c("negative", "positive")[polarity+1]) %>%
            mutate(file=basename(filepath))



# Make intervals ----------------------------------------------------------
ppm <- 15 # +/-


mz_intevals <- data %>%
                select(Name, `[M-H]-`, `[M+H]+`) %>%
                gather("polarity", "mz", -Name) %>%
                mutate(polarity = ifelse(polarity=="[M-H]-", "negative", "positive")) %>%
                mutate(mz_upper = mz + ppm*mz*1E-6, mz_lower = mz - ppm*mz*1E-6)

mz_intevals_mat <- mz_intevals %>%
        select(mz_lower, mz_upper) %>%
        as.matrix


# Make chromatograms ------------------------------------------------------
EICs <- chromatogram(raw_selected, mz = mz_intevals_mat, aggregationFun = "sum", missing = 0)


EICs_long <- .ChromatogramsLongFormat(EICs, pdata = new("NAnnotatedDataFrame", pheno)) %>%
                as_tibble %>%
                dplyr::rename(polarity_file = polarity)


EICs_long <- mz_intevals %>%
                mutate(interval_name = Name, int_idx = 1:n()) %>%
                select(interval_name, int_idx, polarity) %>%
                mutate(interval_name = factor(interval_name, levels = unique(interval_name))) %>%
                right_join(EICs_long, by = "int_idx") %>%
                dplyr::rename(polarity_interval = polarity) %>%
                filter(polarity_interval == polarity_file) %>%
                mutate_if(is.factor, droplevels)



# Save data ---------------------------------------------------------------
saveRDS(EICs,      paste0("_output/", params$general$date,"/data/EICs.rds"))
saveRDS(EICs_long, paste0("_output/", params$general$date,"/data/EICs_long.rds"))

rm(EICs, raw_selected, mz_intevals_mat, gdoc, data, ppm, pheno)
gc()
