library(chemhelper)
library(parallel)
library(multidplyr)
library(purrr)
library(rlist)
library(xcms)
library(dplyr)
library(tidyr)


source( "_funs/drift_correct_with_LOESS_on_QCs_multithread.R"  )


# Funs --------------------------------------------------------------------
XCMSnExp_peaklist_long <- function(XCMSnExp){

    chromPeaks_classic <- function(x){ chromPeaks(x) %>% 
                                        as.data.frame() %>% 
                                        bind_cols(as.data.frame(chromPeakData(x))) %>% 
                                        as_tibble
    }
    
    
    temp_peaks <- chromPeaks_classic(XCMSnExp) %>% 
                        mutate(peakidx = 1:nrow(.)) %>% 
                        dplyr::rename(fromFile = sample) %>% 
                        mutate(filepath = fileNames(XCMSnExp)[fromFile])
    
    
    temp_features <- featureDefinitions(XCMSnExp) %>% 
                        as_tibble() %>% 
                        mutate(feature_id = 1:nrow(.)) %>% 
                        unnest(peakidx)
        
    
    temp_peaks_tab <- featureValues(XCMSnExp, value = "into", method = "maxint") %>% 
                        as_tibble %>% 
                        mutate(feature_id = 1:nrow(.)) %>% 
                        gather("filename", "into_f", -feature_id)
    
    
    
    
    out <- temp_features %>% 
            select(-npeaks, -dummy) %>% 
            rename_at(vars(one_of("mzmed", "mzmin", "mzmax", "rtmed", "rtmin", "rtmax")), funs(paste0("f_", .))) %>% 
            left_join(temp_peaks, by="peakidx") %>% 
            mutate(filename = basename(filepath)) %>% 
            left_join(temp_peaks_tab, by = c("feature_id", "filename"))
    
    
    
    out <- out %>% 
            filter(into == into_f) %>% 
            select(-into_f) %>% 
            group_by(feature_id, filename)%>%
            slice(1) %>%  # xcms sometimes makes the same peak twice so we take the first.
            ungroup
    
    
    return(out)
}




# Make peaklist for pH experiment -----------------------------------------
peaklist_pH_exp_all_long <- map_dfr(x,
                                ~..1 %>% 
                                list.remove("all") %>% 
                                map_dfr(~ XCMSnExp_peaklist_long(..1$pH_exp$pgagf), 
                                        .id = "Eluent"
                                        ),
                         .id = "polarity"
                         ) %>% 
                    mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                    mutate(polarity = ifelse(polarity == "neg","negative",polarity)) %>% 
                    left_join(factors, by = c("filepath", "Eluent"))



peaklist_all_types_all_long <- map_dfr(x,
                                ~..1 %>% 
                                list.remove("all") %>% 
                                map_dfr(~ XCMSnExp_peaklist_long(..1$all_types$pgagf), 
                                        .id = "Eluent"
                                        ),
                         .id = "polarity"
                         ) %>% 
                    mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                    mutate(polarity = ifelse(polarity == "neg","negative",polarity)) %>% 
                    left_join(factors, by = c("filepath", "Eluent"))



# Make peaklist for wine experiment ---------------------------------------
peaklist_wine_long <- map_dfr(x,
                                ~..1 %>% 
                                {list.remove(., names(.) %>% {.!="1 % FA"} %>% which)} %>% 
                                map_dfr(~ XCMSnExp_peaklist_long(..1$all_types$pgagf), 
                                        .id = "Eluent"
                                        ),
                         .id = "polarity"
                         ) %>% 
                    mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                    mutate(polarity = ifelse(polarity == "neg","negative",polarity)) %>% 
                    left_join(factors, by = c("filepath", "Eluent"))



# Make summary of cmp rts -------------------------------------------------
cmp_db <- peaklist_all_types_all_long %>%
            distinct(filename, polarity, Eluent) %>% 
            mutate(filename = gsub(".mzML", "", filename)) %>% 
            right_join(rename(compound_rts, filename = file), by = "filename") %>% 
            group_by(Eluent, Class, Compound) %>% 
            summarise(rt_check = mean(rt, na.rm = TRUE), 
                      mz_neg = mean(mz_neg, na.rm = TRUE), 
                      mz_pos = mean(mz_pos, na.rm = TRUE)
                      ) %>% 
            ungroup %>% 
            filter(!is.nan(rt_check)) %>% 
            gather("polarity", "mz", -Eluent, -Class, -Compound, -rt_check) %>% 
            mutate(polarity = ifelse(polarity == "mz_pos","positive",polarity)) %>%
            mutate(polarity = ifelse(polarity == "mz_neg","negative",polarity))


# Fix RT for moving Ellagic acid
cmp_db <- cmp_db %>% mutate(rt_check = ifelse(Compound=="Ellagic acid" & Eluent == "1 % FA",11,rt_check))



# Annotate to all peaklists -----------------------------------------------
# make list of features
feature_sum <- peaklist_all_types_all_long %>% 
                distinct(polarity, Eluent, feature_id, f_mzmed, f_rtmed)


# loop through conditions and annotate
anno <- list()
anno2 <- list()

for(i in unique(feature_sum$polarity)){
    for(ii in unique(feature_sum$Eluent)){

anno[[i]][[ii]] <- db.comp.assign(mz = feature_sum %>% filter(polarity == i, Eluent == ii) %>% pull(f_mzmed),
                       rt = feature_sum %>% filter(polarity == i, Eluent == ii) %>% pull(f_rtmed) %>% {./60},
                       comp_name_db = cmp_db %>% filter(!is.nan(mz), polarity == i, Eluent == ii) %>% pull(Compound),
                       mz_db =        cmp_db %>% filter(!is.nan(mz), polarity == i, Eluent == ii) %>% pull(mz),
                       rt_db =        cmp_db %>% filter(!is.nan(mz), polarity == i, Eluent == ii) %>% pull(rt_check),
                       mzabs=0.01,
                       ppm=15,
                       ret_tol=0.2
                       ) %>% 
        {tibble(annotation = .)} %>% 
        bind_cols(filter(feature_sum, polarity == i, Eluent == ii))


anno2[[i]][[ii]] <- db.comp.assign(mz = feature_sum %>% filter(polarity == i, Eluent == ii) %>% pull(f_mzmed),
                       rt = feature_sum %>% filter(polarity == i, Eluent == ii) %>% pull(f_rtmed) %>% {./60},
                       comp_name_db = cmp_db %>% filter(!is.nan(mz), polarity == i, Eluent == ii) %>% pull(Class),
                       mz_db =        cmp_db %>% filter(!is.nan(mz), polarity == i, Eluent == ii) %>% pull(mz),
                       rt_db =        cmp_db %>% filter(!is.nan(mz), polarity == i, Eluent == ii) %>% pull(rt_check),
                       mzabs=0.01,
                       ppm=15,
                       ret_tol=0.2
                       ) %>% 
        {tibble(annotation = .)} %>% 
        bind_cols(filter(feature_sum, polarity == i, Eluent == ii))

    }
}



anno <- anno %>% 
            as_tibble %>%
            gather("polarity", "data") %>% 
            unnest %>% 
            select(annotation, polarity, Eluent, feature_id) %>% 
            filter(annotation!="")

anno2 <- anno2 %>% 
            as_tibble %>%
            gather("polarity", "data") %>% 
            unnest %>% 
            select(annotation_class = annotation, polarity, Eluent, feature_id) %>% 
            filter(annotation_class!="")

anno <- anno %>% left_join(anno2, by = c("polarity", "Eluent", "feature_id"))



# copy annotation to peaklists
peaklist_all_types_all_long <- peaklist_all_types_all_long %>% 
                                    left_join(anno, by = c("polarity", "Eluent", "feature_id"))


peaklist_pH_exp_all_long    <- peaklist_pH_exp_all_long %>% 
                                    left_join(anno, by = c("polarity", "Eluent", "feature_id"))


peaklist_wine_long          <- peaklist_wine_long %>% 
                                    left_join(anno, by = c("polarity", "Eluent", "feature_id"))



rm(feature_sum, anno, anno2)






# Drift correction --------------------------------------------------------
cluster <-  create_cluster( detectCores() )# %>%
            # cluster_library("dplyr") %>% 

clusterExport(cluster,c("drift_correct_with_LOESS_on_QCs_vec"))

peaklist_all_types_all_long %<>%     #filter(feature_id %in% 21006) %>% # 21006 bad
                                        mutate(is_QC = grepl("QC", `Sample Type`)) %>%
                                        # group_by(feature_id, polarity) %>%
                                        partition(feature_id, polarity, cluster = cluster) %>%
                                        filter(sum(grepl("QC", `Sample Type`))>=5) %>% # too few QCSs with values and it fails. Since it is only one feature then I just remove
                                        mutate(into_drift_corr = drift_correct_with_LOESS_on_QCs_vec(into, is_QC, time_rank)$x.corrected) %>% 
                                        dplyr::collect() %>%
                                        ungroup

peaklist_pH_exp_all_long %<>%     #filter(feature_id %in% 21006) %>% # 21006 bad
                                        mutate(is_QC = grepl("QC", `Sample Type`)) %>%
                                        # group_by(feature_id, polarity) %>%
                                        partition(feature_id, polarity, cluster = cluster) %>%
                                        filter(sum(grepl("QC", `Sample Type`))>=5) %>% # too few QCSs with values and it fails. Since it is only one feature then I just remove
                                        mutate(into_drift_corr = drift_correct_with_LOESS_on_QCs_vec(into, is_QC, time_rank)$x.corrected) %>% 
                                        dplyr::collect() %>%
                                        ungroup


peaklist_wine_long %<>%     #filter(feature_id %in% 21006) %>% # 21006 bad
                                        mutate(is_QC = grepl("QC", `Sample Type`)) %>%
                                        # group_by(feature_id, polarity) %>%
                                        partition(feature_id, polarity, cluster = cluster) %>%
                                        filter(sum(grepl("QC", `Sample Type`))>=5) %>% # too few QCSs with values and it fails. Since it is only one feature then I just remove
                                        mutate(into_drift_corr = drift_correct_with_LOESS_on_QCs_vec(into, is_QC, time_rank)$x.corrected) %>% 
                                        dplyr::collect() %>%
                                        ungroup


stopCluster(cluster)
rm(cluster)




# Add CAMERA --------------------------------------------------------------

peaklist_pH_exp_all_camera <- map_dfr(x,
                                ~..1 %>% 
                                list.remove("all") %>% 
                                map_dfr(~ getPeaklist(..1$pH_exp$pgagf_xsaFA) %>% 
                                            as_tibble %>% 
                                             mutate(feature_id = 1:nrow(.))
                                        , 
                                        .id = "Eluent"
                                        ),
                         .id = "polarity"
                         ) %>% 
                    mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                    mutate(polarity = ifelse(polarity == "neg","negative",polarity))



peaklist_all_types_all_camera <- map_dfr(x,
                                ~..1 %>% 
                                list.remove("all") %>% 
                                map_dfr(~ getPeaklist(..1$all_types$pgagf_xsaFA) %>% 
                                            as_tibble %>% 
                                             mutate(feature_id = 1:nrow(.))
                                        
                                        , 
                                        .id = "Eluent"
                                        ),
                         .id = "polarity"
                         ) %>% 
                    mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                    mutate(polarity = ifelse(polarity == "neg","negative",polarity))



peaklist_wine_camera <- map_dfr(x,
                                ~..1 %>% 
                                {list.remove(., names(.) %>% {.!="1 % FA"} %>% which)} %>% 
                                map_dfr(~ getPeaklist(..1$all_types$pgagf_xsaFA) %>% 
                                            as_tibble %>% 
                                             mutate(feature_id = 1:nrow(.))
                                        , 
                                        .id = "Eluent"
                                        ),
                         .id = "polarity"
                         ) %>% 
                    mutate(polarity = ifelse(polarity == "pos","positive",polarity)) %>% 
                    mutate(polarity = ifelse(polarity == "neg","negative",polarity))





peaklist_all_types_all_long <- 
    peaklist_all_types_all_camera %>% 
    select(Eluent, polarity, feature_id, isotopes, adduct, pcgroup) %>% 
    right_join(peaklist_all_types_all_long, by = c("Eluent", "polarity", "feature_id"))



peaklist_pH_exp_all_long <- 
    peaklist_pH_exp_all_camera %>% 
    select(Eluent, polarity, feature_id, isotopes, adduct, pcgroup) %>% 
    right_join(peaklist_pH_exp_all_long, by = c("Eluent", "polarity", "feature_id"))



peaklist_wine_long <- 
    peaklist_wine_camera %>% 
    select(Eluent, polarity, feature_id, isotopes, adduct, pcgroup) %>% 
    right_join(peaklist_wine_long, by = c("Eluent", "polarity", "feature_id"))




 




# TODO: check why number of features are not the same!
# Max feature id is ok.


peaklist_pH_exp_all_camera %>% 
    group_by(polarity, Eluent) %>% 
    summarise(n = n())

peaklist_pH_exp_all_long  %>% 
    distinct(polarity, Eluent, feature_id) %>% 
    group_by(polarity, Eluent) %>% 
    summarise(n = n())



peaklist_pH_exp_all_long  %>% 
    distinct(polarity, Eluent, feature_id) %>% 
    group_by(polarity, Eluent) %>% 
    summarise(n = max(feature_id))

